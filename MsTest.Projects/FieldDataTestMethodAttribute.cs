﻿using CsvHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MsTest.Projects
{
    public class FieldDataTestMethodAttribute : DataTestMethodAttribute, ITestDataSource
    {

        public string TestCaseId { get; set; }

        public FieldDataTestMethodAttribute(string testCase)
        {
            TestCaseId = testCase;
        }

        public IEnumerable<object[]> GetData(MethodInfo methodInfo)
        {
            string path = $"DataTests\\TC_{TestCaseId}.csv";

            if (!File.Exists(path)) throw new FileNotFoundException();
            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<object>();
                var convert = ConvertRecords();
                return records.Select(x => convert(x)).ToList();
            }

        }

        private Func<object, object[]> ConvertRecords()
        {
            return (object item) => ((IDictionary<string, object>)item).Values.ToArray();
        }

        public override TestResult[] Execute(ITestMethod testMethod)
        {
            if (testMethod.Arguments.Any(x => x.Equals("CLIENT1")))
            {
                return base.Execute(testMethod);
            }
            else
            {
                return new TestResult[] { new TestResult { Outcome = UnitTestOutcome.Inconclusive, LogOutput = "Test was not executed because of client version." } };
            }
        }

        public string GetDisplayName(MethodInfo methodInfo, object[] data)
        {
            if (data != null)
                return string.Format(CultureInfo.InvariantCulture, "Custom - {0} ({1})", methodInfo.Name, string.Join(",", data));

            return null;
        }
    }
}
