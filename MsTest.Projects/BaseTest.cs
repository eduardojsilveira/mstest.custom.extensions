﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsTest.Projects
{
    public class BaseTest
    {
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInitialize()
        {
            Console.WriteLine($"Starting {TestContext.TestName}" );
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Console.WriteLine($"Finishing {TestContext.TestName} ");
        }
    }
}
